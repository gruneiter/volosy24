'use strict'

$(document).ready(function() {
  $('.js-slider').flexslider({
    animation: "slide",
    selector: ".js-slider__list > .js-slider__item",
    controlNav: false,
    touch: true,
    prevText: "",
    nextText: ""
  });
  $.maskfn.definitions['~']='[+-]';
  $('body').delegate('input[type="tel"], input.phone', 'focus', function(){
      $(this).maskfn('+7 (999) 999-99-99');
  });
  $('.modal').fancybox({

  });
});


var hairExtentionHeaderButtons = document.getElementsByClassName( 'hair-extention-header__button' );
var hairExtentionSignSubheader = document.querySelector( '.hair-extention-sign__header span' );
var hairExtentionSignTypeInput = document.querySelector( '.hair-extention-sign-form__input-type input' );
var htmlElement = document.getElementsByTagName('html')[0];
var hairExtentionSignBottomButton = document.querySelector( '.hair-extention-sign-bottom__button' );

var hairExtentionButtons = Array.from(hairExtentionHeaderButtons);

if(hairExtentionSignBottomButton){
    hairExtentionButtons.push(hairExtentionSignBottomButton);
}

for ( var item of hairExtentionButtons ) {
  item.addEventListener('click', function( event ) {
    event.preventDefault();
    htmlElement.classList.add('no-scroll');
    var forSlice = this.href.indexOf( '#' ) + 1;
    var signWindowType = this.href.slice(forSlice);
    var signWindowTypeText = this.dataset.name;
    hairExtentionSignSubheader.innerText = signWindowTypeText;
    if (signWindowTypeText !== 'наращивание') {
      hairExtentionSignTypeInput.setAttribute('value', signWindowTypeText);
    } else {
      hairExtentionSignTypeInput.setAttribute('value', 'тип не указан');
    }

    var signWindow = document.getElementById('hair-extention-sign');
    signWindow.classList.add('hair-extention-sign_show');
    var closeButton = signWindow.getElementsByClassName('hair-extention-sign__close')[0];
    closeButton.onclick = function () {
      signWindow.classList.remove('hair-extention-sign_show');
      htmlElement.classList.remove('no-scroll');
    }
  });
}

var mobileSwitchers = document.querySelectorAll('.mobile-nav__parent-label');

function mobileSwitch() {
  this.classList.toggle('mobile-nav__parent-label--active')
  this.nextElementSibling.classList.toggle('mobile-nav__sublist--display')
}

for ( var i = 0; i < mobileSwitchers.length; i++ ) {
  mobileSwitchers[i].addEventListener( 'click', mobileSwitch )
}

const btnCart = document.getElementById('btnCart');

btnCart.addEventListener( 'click', e => {
  let target = e.currentTarget;
  e.preventDefault();
  target.classList.toggle('active')
} )

// let headerAddressTabs = () => {
//   let headerAddress = document.querySelector('.header-address');
//   if (headerAddress) {
//     let tabs = Array.from(headerAddress.querySelector('.header-address__tabs').children);
//     let contents = Array.from(headerAddress.querySelector('.header-address__contents').children);
//
//     let headerPhoneDisplay = document.querySelector('.header-contacts .header-contacts__phone');
//     let headerPhonesContent = Array.from(document.querySelectorAll('.header-contacts .header-contacts__phone-hidden'));
//
//     tabs[0].classList.add('header-address__tab--active')
//     contents[0].classList.add('header-address__item--active');
//
//     for ( let i in tabs) {
//       tabs[i].addEventListener('click', (e) => {
//         for (let j in tabs) {
//           tabs[j].classList.remove('header-address__tab--active');
//           contents[j].classList.remove('header-address__item--active');
//         };
//         tabs[i].classList.add('header-address__tab--active')
//         contents[i].classList.add('header-address__item--active');
//
//         headerPhoneDisplay.textContent = headerPhonesContent[i].textContent;
//       });
//     }
//   }
// }
// headerAddressTabs();

const mrcSH = () => {
  const mrcSHTitles = Array.from(document.querySelectorAll('[data-mrc-sh-title]'));
  const mrcSHBodies = Array.from(document.querySelectorAll('[data-mrc-sh-body]'));
  mrcSHTitles.forEach((title) => {
    const itemObj = { name: title, bodies: [] };
    const itemName = title.dataset.mrcShTitle;
    const bodiesList = [];
    mrcSHBodies.forEach((body) => {
      const itemBody = body;
      const bodyName = itemBody.dataset.mrcShBody;
      if (itemName === bodyName) {
        bodiesList.push(itemBody);
        itemBody.style.display = 'none';
      }
    });
    if (bodiesList) {
      itemObj.bodies = bodiesList;
      itemObj.name.addEventListener('click', (e) => {
        e.preventDefault();
        bodiesList.forEach((item) => {
          const currentItem = item;
          currentItem.style.display = currentItem.style.display === 'none' ? '' : 'none';
        });
      });
    }
  });
};

mrcSH();

const tabs = () => {
  const tabsList = Array.from(document.querySelectorAll('.tabs'));
  const tabActivate = (c, labels, tabs) => {
    if (!tabs[c].classList.contains('tab--active')) {
      labels.forEach((item, j) => {
        tabs[j].classList.remove('tab--active');
        labels[j].classList.remove('tabs__label--active');
        return item;
      });
      tabs[c].classList.add('tab--active');
      labels[c].classList.add('tabs__label--active');
    }
    console.log(labels[c]);
  }
  tabsList.forEach((block) => {
    const tl = Array.from(block.querySelectorAll('.tabs__label'));
    const tc = Array.from(block.querySelectorAll('.tab'));
    tl.forEach((label, i) => {
      const tab = tc[i];
      const tabLinks = label.id ? Array.from(document.querySelectorAll(`[href="#${label.id}"]`)) : undefined;
      if (+i === 0) {
        label.classList.add('tabs__label--active');
        tab.classList.add('tab--active');
      }
      label.addEventListener('click', () => tabActivate(i, tl, tc));
      if (tabLinks) {
        tabLinks.forEach((link) => {
          link.addEventListener('click', () => tabActivate(i, tl, tc))
        })
      }
      return label;
    });
    return block;
  });
}

tabs();

const productPropertiesMore = document.querySelector('.product-properties__more a');

productPropertiesMore ? productPropertiesMore.addEventListener('click', (e) => {e.target.style.display = 'none'}) : false;

const lengthSelect = () => {
  const select = document.querySelector('.product-properties__select--length');
  const imageBlock = document.querySelector('.product-photos__main');
  const image = document.querySelector('.product-photos__main a');
  const imageLength = document.querySelector('.product-photos__length');
  const ruler = document.querySelector('.product-photos__ruler');
  const rulerVal = document.querySelector('.product-photos__ruler-value');
  let mainHeight = null;
  if (imageLength) {
    imageLength.style.display = 'none';

    const setRuler = () => {
      let selected = select.options[select.selectedIndex].innerText;
      let s = +`	${selected.match(/(\d*)([\s\S]*)/)[1]}`;
      const doSplit = str => {
        const [, ...arr] = str.match(/(\d*)([\s\S]*)/);
        return str.match(/(\d*)([\s\S]*)/)[1];
      };
      selected = selected.slice(0, s);
      rulerVal.innerText = s;
      ruler.className = `product-photos__ruler product-photos__ruler--${s}`;
    }

    setRuler();

    const animateHeight = (from, to, target, duration) => {
      let start = null;
      let timer = null;
      function tick(timestamp) {
        start = start || timestamp;
        const elapsedTime = timestamp - start;
        const progress = elapsedTime / duration;
        const value = to > from ? from + (to - from) * progress : from - (from - to) * progress
        if(progress >= 1) {
          target.style.height = `${to}px`;
          target.style.height = '';
          return cancelAnimationFrame(timer);
        }
        target.style.height = `${value.toFixed(3)}px`;
        timer = requestAnimationFrame(tick);
      }
      timer = requestAnimationFrame(tick);
    }
    select.addEventListener('mousedown', (e) => {
      mainHeight = imageBlock.offsetHeight;
      image.style.display = 'none';
      imageLength.style.display = 'block';
      ruler.style.display = 'block';
      let imgLHeight = imageBlock.offsetHeight;
      imageBlock.style.height = `${mainHeight}px`;
      animateHeight(mainHeight, imgLHeight, imageBlock, 200);
    })

    select.addEventListener('change', (e) => {
      setRuler();
    })

    select.addEventListener('blur', (e) => {
      const imgLHeight = imageBlock.offsetHeight;
      animateHeight(imgLHeight, mainHeight, imageBlock, 200);
      imageLength.style.display = 'none';
      ruler.style.display = 'none';
      image.style.display = 'block';
      imageLength.style.opacity = 1;
      ruler.style.opacity = 1;
    })
  }
}
lengthSelect();

const instaWidget = (settings = {element: ''}) => {

  class InstaWidget {
    constructor(settings = {
      elem: '',
      userId: '',
      title: '',
    }) {
      this.elem = settings.elem;

      this.userId = settings.userId || '';

      if (this.elem.dataset.title) this.title = this.elem.dataset.title;
      else this.title = settings.title;

      const url = settings.url || {};

      this.url = {
        getName: url.getName || '/instagram-widget/index.php?get_user_name=Y',
        getMedia: url.getMedia || '/instagram-widget/index.php',
        refreshToken: url.refreshToken || '/instagram-widget/index.php?refresh=Y',
      };
    }

    async userName() {
      const response = await fetch(this.url.getName);
      const user = await response.text();
      return user;
    }

    init() {
      new Promise((res) => {
        res(this.userName());
      })
        .then((res) => {
          this.userId = this.userId ? this.userId : res;
          this.title = this.title ? this.title : res;
        })
        .then(() => {
          if (this.elem.children.length > 0 || !this.url) {
            return;
          }
          const list = document.createElement('div');
          const title = document.createElement('div');
          const titleLink = document.createElement('a');
          const filter = this.elem.dataset.tagFilter ? (
            this.elem.dataset.tagFilter
              .split(',')
              .map((item) => `#${item.trim()}`)
          ) : [''];
          const className = 'instagram-widget';
          this.elem.classList.add(className);

          list.classList.add(`${className}__list`);
          title.classList.add(`${className}__title`);
          titleLink.setAttribute('href', `https://www.instagram.com/${this.userId}/`);
          titleLink.setAttribute('target', '_blank');
          titleLink.innerText = `${this.title}`;
          title.appendChild(titleLink);
          this.elem.appendChild(title);

          fetch(this.url.getMedia)
            .then((response) => response.json())
            .then((response) => response.data.filter((item) => filter.map((filterItem) => item.caption.includes(filterItem)).filter((i) => i === true).length > 0)) // eslint-disable-line max-len
            .then((result) => {
              const data = result.slice(0, 8);
              data.forEach((item) => {
                const inwidgetItem = document.createElement('div');
                const inwidgetItemLink = document.createElement('a');
                const inwidgetItemImage = document.createElement('img');
                inwidgetItem.className = `${className}__item`;
                inwidgetItemLink.href = item.media_type === 'VIDEO' ? item.thumbnail_url : item.media_url;
                inwidgetItemLink.dataset.fancybox = 'inwidget';
                inwidgetItemImage.src = item.media_type === 'VIDEO' ? item.thumbnail_url : item.media_url;
                inwidgetItemImage.alt = item.caption;
                inwidgetItemLink.appendChild(inwidgetItemImage);
                inwidgetItem.appendChild(inwidgetItemLink);
                list.appendChild(inwidgetItem);
              });
              this.elem.appendChild(list);
            })
            .catch(() => {
              this.elem.remove();
            });
        });
    }
  }
  const selector = settings.element || '.instagram-widget';
  const inwidget = Array.from(document.querySelectorAll(selector));

  inwidget.forEach((item) => {
    const widget = new InstaWidget({ 'elem': inwidget });
    widget.init();
  })
}

instaWidget();;
