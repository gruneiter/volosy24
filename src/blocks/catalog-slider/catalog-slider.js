import Swiper, { Navigation } from 'swiper';

const catalogSlider = () => new Swiper('.catalog-slider__swiper', {
  modules: [Navigation],
  loop: true,
  speed: 200,
  spaceBetween: 15,
  loopedSlides: 3,
  breakpoints: {
    480: {
      slidesPerView: 3,
    },
    800: {
      slidesPerView: 4,
    },
    1000: {
      slidesPerView: 5,
      spaceBetween: 30,
    },
  },

  navigation: {
    nextEl: '.catalog-slider__button--next',
    prevEl: '.catalog-slider__button--prev',
  },
});

export default catalogSlider;
