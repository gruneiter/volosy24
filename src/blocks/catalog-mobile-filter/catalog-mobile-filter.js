const mobileFilter = document.querySelector('.catalog-mobile-filter');

const addMobileFilter = () => {
  if (!mobileFilter) return;
  const filterButton = mobileFilter.querySelector('.catalog-mobile-filter__button');
  filterButton.addEventListener('click', () => {
    filterButton.closest('div').classList.toggle('catalog-mobile-filter__block--active');
  });
};

export default addMobileFilter;
