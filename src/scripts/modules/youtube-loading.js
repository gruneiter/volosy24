const loadVideo = (block, id) => {
  block.insertAdjacentHTML('beforeend', `<iframe src="https://www.youtube.com/embed/${id}" allowfullscreen>`);
};

const youtubeLoading = () => {
  const blocks = document.querySelectorAll('.youtube-responsive__iframe');
  const blocksToLoad = [];
  if (!blocks) return;
  Array.from(blocks).forEach((block) => {
    if (block.dataset.ytId && !block.firstElementChild) blocksToLoad.push(block);
  });

  blocksToLoad.forEach((block) => {
    const handler = () => {
      loadVideo(block, block.dataset.ytId);
      window.removeEventListener('scroll', handler);
      window.removeEventListener('mousemove', handler);
    };
    window.addEventListener('scroll', handler);
    window.addEventListener('mousemove', handler);
  });
};

export default youtubeLoading;
